
import agenda.Pessoa;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PessoaTest {
    
    private Pessoa pessoaTest;
    
    public PessoaTest() {
    }      
    
    @Before
    public void setUp() {
        pessoaTest = new Pessoa();
    }
    
    @Test
 	
	public void testNome() {       
		pessoaTest.setNome("Rafael"); 			
		assertEquals(pessoaTest.getNome(), "Rafael");
	}
        
        public void testTelefone() {		
		pessoaTest.setTelefone("9999-8888"); 			
		assertEquals(pessoaTest.getTelefone(), "9999-8888");
	}
        
        public void testEmail() {		
		pessoaTest.setNome("exemplo@tal.com"); 			
		assertEquals(pessoaTest.getNome(), "exemplo@tal.com");
	}
    
}
